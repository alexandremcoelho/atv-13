import logo from "./logo.svg";
import "./App.css";
import { member } from "./components/array";
import { Switch, Route, Link, Router } from "react-router-dom";
import { Links } from "./components/Links";
import { Client } from "./components/Body/cliente";
import { Empresa } from "./components/Body/empresa";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Route exact path="/">
          <div>
            {member.map((member, index) => (
              <Links key={index} list={member} />
            ))}
          </div>
        </Route>
        <Switch>
          <Route path="/customer/:id">
            <Client member={member} />
          </Route>
          <Route path="/company/:id">
            <Empresa member={member} />
          </Route>
        </Switch>
      </header>
    </div>
  );
}

export default App;
