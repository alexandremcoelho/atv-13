import { useParams, Link } from "react-router-dom";

export const Empresa = ({ member }) => {
  const { id } = useParams();
  console.log(member);

  return (
    <div>
      <h1>Detalhes da Empresa</h1>

      <p>Nome da empresa: {member[id - 1].name}</p>

      <Link to="/">Voltar</Link>
    </div>
  );
};
