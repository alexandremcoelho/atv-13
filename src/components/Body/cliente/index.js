import { useParams, Link } from "react-router-dom";

export const Client = ({ member }) => {
  const { id } = useParams();
  console.log(member);

  return (
    <div>
      <h1>Detalhes do cliente</h1>

      <p>Nome: {member[id - 1].name}</p>

      <Link to="/">Voltar</Link>
    </div>
  );
};
