import { Link } from "react-router-dom";

export const Links = ({ list }) => {
  console.log(list.name);
  return (
    <div>
      {list.type === "pf" ? (
        <Link to={`/customer/${list.id}`}>{list.name}</Link>
      ) : (
        <Link to={`/company/${list.id}`}>{list.name}</Link>
      )}
    </div>
  );
};
